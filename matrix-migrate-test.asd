(asdf:defsystem "matrix-migrate-test"
  :version "0.0"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "AGPL V3+"
  :depends-on ("matrix-migrate" "parachute" "jsown")
  :components ((:module "test"
                        :components
                        ((:file "test")
                         (:file "run")))))
