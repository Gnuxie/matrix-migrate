# matrix-migrate

## what is matrix-migrate?
matrix-migrate is a script that allows you to transfer rooms from one account matrix account to another, this includes across homeservers.

matrix-migrate will try op your new account to the same level as your old account and will send a message into the room if it cannot restore your power level.

It can be used for adding existing rooms to an alternate account or to transfer accounts completly.

## ok so how do I use it?

### installation
make sure you have a Common Lisp implementation installed, if you don't know what that means just install sbcl.

if you're going to use the bash script use sbcl or edit the script to work with your preferred implementation.

make sure you have quicklisp installed.

clone this repo into your quicklisp local projects

`git clone https://gitlab.com/Gnuxie/matrix-migrate.git ~/quicklisp/local-projects/matrix-migrate`

### the unix way
simply do `chmod +x ./matrix-migrate.sh`
then
`matrix-migrate <my-current-username e.g. "@me:matrix.org"> <my-password (quoted)> <my-new-username> <my-new-password>`

### the lisp way
simply load the package either with quicklisp or asdf then do

`(matrix-migrate:password-migrate my-current-user-id my-current-pass my-next-user-id my-next-pass)`

or

`(matrix-migrate:token-migrate my-user-id my-token my-other-id my-other-token)`

these both take optional arguments leave and forget which will tell matrix-migrate to leave or forget the rooms in the current user after it has finished.

## ok is this safe?
at current so long as nothing unexpected happens it should be safe

matrix-migrate will not leave rooms unless you have told it to, it will also not leave rooms unless it has verified that the new account is in them.

It will not leave rooms unless the power level has been verified also.
