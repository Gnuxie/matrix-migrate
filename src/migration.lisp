#| This file is part of matrix-migrate
   Copyright (C) 2018-2019 Gnuxie <Gnuxie@protonmail.com> |#
(in-package :cl-user)
(defpackage :matrix-migrate
  (:use :cl)
  (:export
   
   :migrate-room
   :migrate
   :password-migrate
   :token-migrate))

(in-package :matrix-migrate)

(defparameter *skipped-rooms* nil)
(defparameter *invite-queue* nil)

(defun send-advert (room)
  (cl-matrix:msg-send "This account was migrated using https://gitlab.com/Gnuxie/matrix-migrate" room :type "m.notice"))

(defmacro sanatise-numbers (&rest numbers)
  "used to sanatise the power levels recieved by turning them to integers as synapse is not spec conforming"
  `(progn
     ,@ (loop for n in numbers collect `(etypecase ,n (integer ,n) (string (setf ,n (parse-integer ,n)))))))

(declaim (inline get-user-from-levels))
(defun get-user-from-levels (user-id power-levels)
  "return the user power level, if the user is not present then the default power level will be assumed

returns another boolean value which is t if the default-level was given"
  (let ((users (jsown:filter power-levels "users"))
        (default-level (jsown:filter power-levels "users_default")))
    (if (jsown:keyp users user-id)
        (values (jsown:val users user-id) nil)
        (values default-level t))))

(declaim (inline can-restore-p))
(defun can-restore-p (user-level power-levels)
  "returns t if the user is able to change thier power level."
  (let ((required-level (jsown:filter power-levels "events" "m.room.power_levels")))
    (sanatise-numbers user-level required-level)
    (>= user-level required-level)))

(declaim (iniline can-invite-p))
(defun can-invite-p (user-level power-levels)
  "returns t if a user of this power is able to invite other users."
  (let ((invite-level (jsown:filter power-levels "invite")))
    (sanatise-numbers user-level invite-level)
    (>= user-level invite-level)))

(defun invite-only? (room-id)
  "assert that the room given has the invite join rule."
  (let ((join-rule (jsown:val (cl-matrix:room-state room-id "m.room.join_rules") "join_rule")))
    (string= join-rule "invite")))

(defun lteq-power (user-one user-two power-levels)
  "test whether the first user has less or the same power as the second user."
  (<= (get-user-from-levels (cl-matrix:username user-one)  power-levels)
      (get-user-from-levels (cl-matrix:username user-two) power-levels)))

(defun users-of-power (power-levels)
  "return the user-ids of those with enough power to change the room power levels."
  (let ((required-level (jsown:filter power-levels "events" "m.room.power_levels")))
    (mapcar #'car (remove-if-not #'(lambda (x) (>= (cdr x) required-level))
                                 (cdr (jsown:val power-levels "users"))))))

(defun migrate-invite-and-restore (current-user next-user room-id power-levels)
  "invite the user to the room and restore their power level."

  (cl-matrix:with-account (current-user)
    (cl-matrix:user-invite (cl-matrix:username next-user) room-id)
    (if (invite-only? room-id)
        (progn
          (format t "Invite only room, adding to invite queue")
          (push room-id *invite-queue*))
        
        (cl-matrix:with-account (next-user)
          (cl-matrix:room-join room-id)))

    ; unless the current user has a lower or the same power level as the next user.
    (unless (lteq-power current-user next-user power-levels)
      
      (cl-matrix:change-power-level room-id
                                    (cl-matrix:username next-user)
                                    (jsown:filter power-levels
                                                  "users" (cl-matrix:username current-user))))
    
    (send-advert room-id)))

(defun migrate-invite-and-msg (current-user next-user room-id power-levels)
  "Invite the user to the room and attempt to message the moderators to restore the power level."

  (cl-matrix:with-account (current-user)
    (cl-matrix:user-invite (cl-matrix:username next-user) room-id)
    (unless (lteq-power current-user next-user power-levels)
      (format t "~%not default, attempting to notify the moderators")
      (let ((current-level (get-user-from-levels (cl-matrix:username current-user) power-levels)))
        (cl-matrix:msg-send (format nil "Hi ~{~a,~} I am migrating my account to ~a please could you restore my power level of ~d.~%~%Thank you."
                                    (users-of-power power-levels)
                                    (cl-matrix:username next-user)
                                    current-level)
                            room-id)))

    (if (invite-only? room-id)
        (progn
          (format t "~%Invite only room, adding to invite queue~%")
          (push room-id *invite-queue*))
        (cl-matrix:with-account (next-user)
          (cl-matrix:room-join room-id)))

    (send-advert room-id)))

(defun migrate-room (current-account next-account room-id)
  "migrate a room from one account to another."
  (cl-matrix:with-account (current-account)
    (let ((power-levels (cl-matrix:room-state room-id "m.room.power_levels")))
      (multiple-value-bind (current-level defaultp) (get-user-from-levels (cl-matrix:username current-account)
                                                                           power-levels)

        (format t "~%~%current-level can-invite is-default ~%~8d~16@a~11@a~%" current-level defaultp
                (can-invite-p current-level power-levels))
        
        (cond ((and (can-restore-p current-level power-levels)
                    (can-invite-p current-level power-levels))
               (format t "~%Attempting to invite & Restore ~s" room-id)
               (migrate-invite-and-restore current-account next-account room-id power-levels))

              ((can-invite-p current-level power-levels)
               (format t "~%Attempting to invite ~s" room-id)
               (migrate-invite-and-msg current-account next-account room-id power-levels))

              (t (format t "~%Skipping room ~s" room-id) (push room-id *skipped-rooms*)))))))

(defun wait-on-invite ()
  "wait for all the invitations to have been recieved for the rooms in the invite queue before joining them."
  (loop :while *invite-queue* :do
       (format t "invite queue ~a~%" *invite-queue*)
       (jsown:do-json-keys (room-id room-data) (cl-matrix:invitations)
         (when (member room-id *invite-queue* :test #'string=)
           (format t "room found, joining ~a~%" room-id)
           (cl-matrix:room-join room-id)
           (setf *invite-queue* (delete room-id *invite-queue* :test #'string=))))
       (format t "~%waiting for ~d invitations~%" (length *invite-queue*))
       (sleep 5)))

(defun leave-successful-rooms (current-user next-user rooms &optional leave forget)
  "verify whether the migration was a success

verifies that the power levels have changed also."
  (cl-matrix:with-account (next-user)
    (let ((next-rooms (cl-matrix:user-joined-rooms))
          (bad-rooms nil))
      (dolist (room-id rooms)
        (let ((power-levels (cl-matrix:room-state room-id "m.room.power_levels")))
          ;; verify that the next-user is in the same rooms.
          (if (and (member room-id next-rooms :test #'string=)
                   ;; verify that the power level has been transfered where possible.
                   (or (lteq-power current-user next-user power-levels)
                       (not (can-restore-p (get-user-from-levels (cl-matrix:username current-user) power-levels) power-levels))))
              (when leave
                (cl-matrix:with-account (current-user)
                  (cl-matrix:room-leave room-id)
                  (when forget
                    (cl-matrix:room-forget room-id))))

              (push room-id bad-rooms))))
      bad-rooms)))

(defun migrate (current-account next-account &optional leave forget)
  (cl-matrix:with-account (current-account)
    (let ((rooms (cl-matrix:user-joined-rooms)))
      (dolist (room-id rooms)
        (migrate-room current-account next-account room-id))

      (format t "joining rooms invited to~%")
      (cl-matrix:with-account (next-account)
        (wait-on-invite))

      (let ((bad-rooms (leave-successful-rooms current-account next-account rooms leave forget)))
        (unless (null bad-rooms)
          (format t "~%migration unsuccesful for ~d rooms~%~a~%" (length bad-rooms) bad-rooms))))))

(opts:define-opts
    (:name :help
           :description "print this help text"
           :short #\h
           :long "help")

    (:name :leave
           :description "leave the rooms after completion"
           :short #\l
           :long "leave")

    (:name :forget
           :description "forget the rooms after completion"
           :short #\f
           :long "forget"))

(defun main-unix ()
  (multiple-value-bind (options free-args) (opts:get-opts)
    (if (getf options :help)
        (progn
          (opts:describe
           :prefix "matrix-migrate. Usage:"
           :args "CURRENT-ACCOUNT-ID PASSWORD NEXT-ACCOUNT-ID PASSWORD [keywords]")
          (opts:exit))
        (progn
          (password-migrate (nth 0 free-args) (nth 1 free-args) (nth 2 free-args) (nth 3 free-args) (getf options :leave) (getf options :forget))
          (opts:exit)))))

(defun password-migrate (current-user current-pass next-user next-pass &optional leave forget)
  "migrate accounts logging in with the user id'd and passwords"
  (let ((current-account (cl-matrix:login current-user current-pass))
        (next-account    (cl-matrix:login next-user next-pass)))

    (migrate current-account next-account leave forget)
    (format t "~%Complete Skipped ~d rooms" (length *skipped-rooms*))))

(defun token-migrate (current-user current-token next-user next-token &optional leave forget)
  "migrate accounts by using access tokens to access the account."
  (let ((current-account (cl-matrix:make-account current-user current-token))
        (next-account    (cl-matrix:make-account next-user    next-token)))

    (migrate current-account next-account leave forget)
    (format t "~%Complete Skipped ~d rooms" (length *skipped-rooms*))))
