(defsystem "matrix-migrate"
  :version "0.0"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "AGPL v3+"
  :depends-on ("jsown" "cl-matrix" "unix-opts" "cl-ppcre")
  :components ((:module "src"
                        :components
                        ((:file "migration"))))
  :description "A Script to migrate matrix accounts")
