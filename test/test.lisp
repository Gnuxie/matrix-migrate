#| This file is part of matrix-migrate
   Copyright (C) 2018-2019 Gnuxie <Gnuxie@protonmail.com> |#
(in-package :cl-user)
(defpackage :matrix-migrate-test
  (:use :cl :parachute)
  (:export
   :run
   :ci-run))
(in-package :matrix-migrate-test)

(defparameter *direct-chat* nil)
(defparameter *general-room* nil)

(defparameter *current-user* nil)
(defparameter *next-user* nil)
(defparameter *neutral-user* nil)

(defun load-config ()
  (with-open-file (in (merge-pathnames "test/test.config"
                                       (asdf:system-source-directory :matrix-migrate)))
    (with-standard-io-syntax
        (let ((config (read in)))
          (setf *current-user* (cl-matrix:login (getf config :username)  (getf config :password)))
          (setf *next-user*    (cl-matrix:login (getf config :username2) (getf config :password2)))
          (setf *neutral-user* (cl-matrix:login (getf config :username3)  (getf config :password3)))))))

(defun leave-forget-all-rooms (&rest accounts)
  (dolist (account accounts)
    (cl-matrix:with-account (account)
      (dolist (room  (cl-matrix:user-joined-rooms))
        (format t "~a~%" room)
        (cl-matrix:room-leave room)
        (cl-matrix:room-forget room))
)))

(defun set-up-test ()
  (load-config)
  (leave-forget-all-rooms *current-user* *next-user* *neutral-user*)
  (cl-matrix:with-account (*current-user*)
    (setf *direct-chat* (cl-matrix:room-create :name "migration-test"
                                               :topic "a room where we are admin"
                                               :is-direct t
                                               :preset "trusted_private_chat")))

  (cl-matrix:with-account (*neutral-user*)
    (setf *general-room* (cl-matrix:room-create :name "migration-test neutral"
                                                :topic "a room where the current user isn't admin"))

    (cl-matrix:user-invite (cl-matrix:username *current-user*) *general-room*)
    (cl-matrix:change-power-level *general-room* (cl-matrix:username *current-user*) 20))

  (cl-matrix:with-account (*current-user*)
    (cl-matrix:room-join *general-room*)))

(define-test matrix-migrate-test

  (set-up-test)
  (let ((rooms-before (cl-matrix:with-account (*current-user*)
                        (cl-matrix:user-joined-rooms))))
    (format t "TEST rooms before ~a~%" rooms-before)
    (matrix-migrate:password-migrate (cl-matrix:username *current-user*)
                                     (cl-matrix:password *current-user*)
                                     (cl-matrix:username *next-user*)
                                     (cl-matrix:password *next-user*)
                                     t
                                     t)

    (let ((rooms-next (cl-matrix:with-account (*next-user*)
                        (cl-matrix:user-joined-rooms))))

      (format t "TEST rooms next ~a~%" rooms-next)
      (false (set-difference rooms-before rooms-next :test #'string=)
             "test that the rooms the next user has are the same as the current user did have.")))

  (leave-forget-all-rooms *current-user* *next-user* *neutral-user*))




