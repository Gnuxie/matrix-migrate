(in-package :matrix-migrate-test)

(defun run (&key (report 'plain))
  (let ((full-test-report nil))

    (unwind-protect (setf full-test-report
                          (test 'matrix-migrate-test :report report))
      full-test-report)))

(defun ci-run ()
  (let ((test-result (run)))
    (when (not (null (results-with-status ':FAILED test-result)))
      (uiop:quit -1))))
